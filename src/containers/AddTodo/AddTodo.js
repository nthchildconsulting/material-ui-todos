import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addTodo } from '../../redux/actions/TodosActions';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  button: {
    [theme.breakpoints.down('xs')]: {
      width: '35px',
      height: '35px',
    },
  },
});

class AddTodo extends Component {
  state = {
    todo: '',
  };
  handleChange = event => {
    this.setState({ todo: event.target.value });
  };
  handleClick = () => {
    if (!this.state.todo.trim()) {
      return;
    }
    this.props.dispatch(addTodo(this.state.todo));
    this.setState({ todo: '' });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Grid container spacing={24} justify="center">
          <Grid item xs={10} sm={10}>
            <Input
              fullWidth
              placeholder="What will you do next?"
              value={this.state.todo}
              onChange={this.handleChange}
              onKeyPress={e => {
                if (e.key === 'Enter') {
                  e.preventDefault();
                  this.handleClick();
                }
              }}
            />
          </Grid>
          <Grid item xs={2}>
            <Button
              variant="fab"
              color="primary"
              aria-label="add"
              className={classes.button}
              onClick={this.handleClick}
            >
              <AddIcon />
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}

AddTodo.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect()(withStyles(styles)(AddTodo));
