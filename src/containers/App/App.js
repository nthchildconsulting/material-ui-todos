import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Footer from '../../containers/Footer/Footer';
import AddTodo from '../../containers/AddTodo/AddTodo';
import VisibilityTodoList from '../../containers/VisibilityTodoList/VisibilityTodoList';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    padding: theme.spacing.unit * 6,
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing.unit * 2,
    },
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class App extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={24} justify="center">
          <Grid item xs={12} sm={10} md={8}>
            <Paper className={classes.paper}>
              <AddTodo />
              <VisibilityTodoList />
            </Paper>
          </Grid>
        </Grid>
        <Footer />
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
