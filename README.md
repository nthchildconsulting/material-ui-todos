## Getting Started

Clone the repo to begin.

```
git clone git@gitlab.com:nthchildconsulting/material-ui-todos.git
```

Install the needed node modules.

```
npm install
```

Start the React script.

```
npm start
```

Test the components.

```
npm run test
```

Build the app for production.

```
npm run build
```