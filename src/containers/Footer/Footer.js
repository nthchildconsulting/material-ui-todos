import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  VisibilityFilters,
  setVisibilityFilter,
} from '../../redux/actions/TodosActions';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import DoneIcon from '@material-ui/icons/Done';
import CloudQueueIcon from '@material-ui/icons/CloudQueue';
import ScheduleIcon from '@material-ui/icons/Schedule';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    width: '100%',
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    position: 'fixed',
    left: '0',
    bottom: '0',
    backgroundColor: theme.palette.background.paper,
  },
});

class Footer extends Component {
  state = {
    value: 0,
  };
  handleChange = (event, value) => {
    this.setState({ value });
    let filter = VisibilityFilters.SHOW_ALL;
    switch (value) {
      case 0:
        filter = VisibilityFilters.SHOW_ALL;
        break;
      case 1:
        filter = VisibilityFilters.SHOW_ACTIVE;
        break;
      case 2:
        filter = VisibilityFilters.SHOW_COMPLETED;
        break;
      default:
        filter = VisibilityFilters.SHOW_ALL;
    }
    this.props.dispatch(setVisibilityFilter(filter));
  };
  render() {
    const { value } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <BottomNavigation
          value={value}
          onChange={this.handleChange}
          showLabels
          justify="center"
        >
          <BottomNavigationAction label="All" icon={<CloudQueueIcon />} />
          <BottomNavigationAction label="Active" icon={<ScheduleIcon />} />
          <BottomNavigationAction label="Completed" icon={<DoneIcon />} />
        </BottomNavigation>
      </div>
    );
  }
}

export default connect()(withStyles(styles)(Footer));
