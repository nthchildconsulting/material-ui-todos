import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../TodosActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Visibility Filter Actions', () => {
  it('should create an action to set a visibility filter', () => {
    const filter = actions.VisibilityFilters.SHOW_ACTIVE;
    const expectedActions = [
      {
        type: types.SET_VISIBILITY_FILTER,
        filter,
      },
    ];
    const store = mockStore([]);
    store.dispatch(actions.setVisibilityFilter(filter));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
