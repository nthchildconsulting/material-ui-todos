import VisibilityFilterReducer from '../VisibilityFilterReducer';
import * as actions from '../../actions/TodosActions';
import * as types from '../../constants/ActionTypes';
import deepFreeze from 'deep-freeze';

describe('Visibility Filter Reducer', () => {
  it('should return the initial state', () => {
    expect(VisibilityFilterReducer(undefined, {})).toEqual('SHOW_ALL');
  });
  it('should set visibility filter', () => {
    const beforeState = actions.VisibilityFilters.SHOW_ALL;
    const afterState = actions.VisibilityFilters.SHOW_ACTIVE;
    const action = {
      type: types.SET_VISIBILITY_FILTER,
      filter: actions.VisibilityFilters.SHOW_ACTIVE,
    };
    deepFreeze(beforeState);
    expect(VisibilityFilterReducer(beforeState, action)).toEqual(afterState);
  });
});
