import { combineReducers } from 'redux';
import TodosReducer from './TodosReducer';
import VisibilityFilterReducer from './VisibilityFilterReducer';

export default combineReducers({
  todos: TodosReducer,
  visibilityFilter: VisibilityFilterReducer,
});
