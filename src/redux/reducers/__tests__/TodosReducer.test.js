import TodosReducer from '../TodosReducer';
import * as types from '../../constants/ActionTypes';
import deepFreeze from 'deep-freeze';

describe('Todos Reducer', () => {
  it('should return the initial state', () => {
    expect(TodosReducer(undefined, {})).toEqual([]);
  });
  it('should add a todo', () => {
    const id = 0;
    const text = 'First Todo';
    const beforeState = [];
    const afterState = [
      {
        id,
        completed: false,
        text,
      },
    ];
    const action = { type: types.ADD_TODO, id, text };
    deepFreeze(beforeState);
    expect(TodosReducer(beforeState, action)).toEqual(afterState);
  });
  it('should toggle a todo', () => {
    const id = 0;
    const text = 'First Todo';
    const beforeState = [
      {
        id,
        completed: false,
        text,
      },
    ];
    const afterState = [
      {
        id,
        completed: true,
        text,
      },
    ];
    const action = { type: types.TOGGLE_TODO, id: 0 };
    deepFreeze(beforeState);
    expect(TodosReducer(beforeState, action)).toEqual(afterState);
  });
});
