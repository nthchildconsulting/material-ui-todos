import { VisibilityFilters } from '../actions/TodosActions';
import * as types from '../constants/ActionTypes';

const VisibilityFilterReducer = (
  state = VisibilityFilters.SHOW_ALL,
  action
) => {
  switch (action.type) {
    case types.SET_VISIBILITY_FILTER:
      return action.filter;
    default:
      return state;
  }
};

export default VisibilityFilterReducer;
